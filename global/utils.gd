extends Node


const SAVE_PATH = "res://savegame.bin"

func savegame():
	var file = FileAccess.open(SAVE_PATH, FileAccess.WRITE)
	var data: Dictionary = {
		"playerhealth": game.playerhealth,
		"gold" : game.gold,
	}
	var jstr = JSON.stringify(data)
	file.store_line(jstr)
	
func loadgame():
	var file = FileAccess.open(SAVE_PATH, FileAccess.READ)
	if FileAccess.file_exists(SAVE_PATH):
		if not file.eof_reached():
			var current_line = JSON.parse_string(file.get_line())
			if current_line:
				game.playerhealth == current_line["playerhealth"]
				game.gold = current_line["gold"]
